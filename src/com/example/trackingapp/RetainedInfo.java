package com.example.trackingapp;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class RetainedInfo  implements Parcelable {

	private boolean onTracking;
	private String stringFile;
	private boolean anyTrackSeg;
	
	public RetainedInfo () {
	      onTracking = false;
	      anyTrackSeg = false;
	      stringFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
	  			"<gpx version=\"1.1\" creator=\"Endomondo.com\"" +
				" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1" +
				" http://www.topografix.com/GPX/1/1/gpx.xsd" +
				" http://www.garmin.com/xmlschemas/GpxExtensions/v3" +
				" http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd" +
				" http://www.garmin.com/xmlschemas/TrackPointExtension/v1" +
				" http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\"" +
				" xmlns=\"http://www.topografix.com/GPX/1/1\"" +
				" xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"" +
				" xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\"" +
				" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<trk>";
	}
	
	
	public RetainedInfo (Parcel in) {
		   this();
		   readFromParcel(in); 
	}
	
	private void readFromParcel(Parcel in) {
		onTracking = in.readInt() == 1;
		anyTrackSeg = in.readInt() == 1;
		in.readInt();
		in.readString();
		
	}


	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(onTracking ? 1 : 0);
		dest.writeInt(anyTrackSeg ? 1 : 0);
		dest.writeString(stringFile);
	}
	/**
	 * @return the onTracking
	 */
	public boolean isOnTracking() {
		return onTracking;
	}
	/**
	 * @param onTracking the onTracking to set
	 */
	public void setOnTracking(boolean onTracking) {
		this.onTracking = onTracking;
	}
	
	public void changeTrackingOnOff(){
		this.onTracking = !this.onTracking;
	}
	
	public String getStringFile(){
		return stringFile;
	}
	
	public void startSeg(){
		this.stringFile = this.stringFile + "<trkseg>\n";
	}
	
	public void endSeg(){
		this.stringFile = this.stringFile + "</trkseg>\n";
	}
	
	@SuppressLint("SimpleDateFormat") public void insertNewPointTrack(Location l){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		this.stringFile = this.stringFile + "<trkpt lat=\"" + l.getLatitude() + "\" lon=\"" + l.getLongitude() + "\"><time>" + df.format(new Date(l.getTime())) + "</time></trkpt>\n";
	}
	
	
	public void finishWritingFile(File file){
		if(this.onTracking)
			this.stringFile = this.stringFile + "</trkseg></trk></gpx>";
		else
			this.stringFile = this.stringFile + "</trk></gpx>";
		try {
            FileWriter writer = new FileWriter(file, false);
            writer.append(stringFile);
            writer.flush();
            writer.close();
        } catch (IOException e) {
        }
	}
	
	public void resetString(){
		this.stringFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
	  			"<gpx version=\"1.1\" creator=\"Endomondo.com\"" +
				" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1" +
				" http://www.topografix.com/GPX/1/1/gpx.xsd" +
				" http://www.garmin.com/xmlschemas/GpxExtensions/v3" +
				" http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd" +
				" http://www.garmin.com/xmlschemas/TrackPointExtension/v1" +
				" http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\"" +
				" xmlns=\"http://www.topografix.com/GPX/1/1\"" +
				" xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\"" +
				" xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\"" +
				" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
				"<trk>";
	}
	
	public void resetAnyTrackSeg(){
		this.anyTrackSeg = false;
	}
	
	public void thereWasTrackSeg(){
		this.anyTrackSeg = true;
	}
	
	public boolean getAnyTrackSeg(){
		return this.anyTrackSeg;
	}
}