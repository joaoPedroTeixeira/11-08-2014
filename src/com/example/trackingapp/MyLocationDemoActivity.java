/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.trackingapp;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.trackingapp.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;


public class MyLocationDemoActivity extends FragmentActivity implements ConnectionCallbacks,OnConnectionFailedListener,LocationListener,OnMyLocationButtonClickListener {

	private GoogleMap mMap;
	private LocationClient mLocationClient;
	private RetainedInfo retainedInfo;
	private static final LocationRequest REQUEST = LocationRequest.create().setInterval(5000).setFastestInterval(16).setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_location_demo);
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		if (savedInstanceState == null) {
            mapFragment.setRetainInstance(true);
            retainedInfo = new RetainedInfo();
        } else {
            mMap = mapFragment.getMap();
            retainedInfo = savedInstanceState.getParcelable("LOCATIONS");
        }
        setUpMapIfNeeded();
	}
	
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("LOCATIONS", retainedInfo);
        super.onSaveInstanceState(outState);
    }
	
	
	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		setUpLocationClientIfNeeded();
		mLocationClient.connect();
	}

	
	private void setUpMapIfNeeded() {
		if (mMap == null) {
		    mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		    if (mMap != null) {
		        mMap.setMyLocationEnabled(true);
		        mMap.setOnMyLocationButtonClickListener(this);
		    }
		}
	}
	
	private void setUpLocationClientIfNeeded() {
		if (mLocationClient == null) {
		    mLocationClient = new LocationClient(getApplicationContext(), this,this); 
		}
	}
	
	/**
	* Implementation of {@link LocationListener}.
	*/
	@Override
	public void onLocationChanged(Location location) {
		if(retainedInfo.isOnTracking()){
			Location loc = mLocationClient.getLastLocation();
		    retainedInfo.insertNewPointTrack(loc);
		}
	}
	
	/**
	* Callback called when connected to GCore. Implementation of {@link ConnectionCallbacks}.
	*/
	@Override
	public void onConnected(Bundle connectionHint) {
		mLocationClient.requestLocationUpdates(REQUEST,this);
		
	}
	
	@Override
	public boolean onMyLocationButtonClick() {
		Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
		return false;
	}
	
	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}
	
	public boolean startPauseClick(View view){
		if(!retainedInfo.isOnTracking()){
			retainedInfo.startSeg();
			retainedInfo.setOnTracking(true);
			findViewById(R.id.button_play_pause).setBackgroundResource(R.drawable.pausev2);
			retainedInfo.thereWasTrackSeg();
		}else{
			retainedInfo.endSeg();
			retainedInfo.setOnTracking(false);
			findViewById(R.id.button_play_pause).setBackgroundResource(R.drawable.playv2);
		}
		return false;
	}
	
	@SuppressLint("SimpleDateFormat") public boolean stopTracking(View view){
		if(retainedInfo.getAnyTrackSeg()){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
			String currentDateandTime = sdf.format(new Date());
			retainedInfo.finishWritingFile((new File(Environment.getExternalStorageDirectory(),
					   "TRACKING"+currentDateandTime+".gpx")));
			retainedInfo.resetString();
			retainedInfo.setOnTracking(false);
			retainedInfo.resetAnyTrackSeg();
		}
		return false;
	}


	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
	
}

